<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;
use Src\controllers\Dog;
use Src\helpers\Helpers;

class BookingTest extends TestCase {

	private $booking;
    private $dogs;
    private $helper;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->booking = new Booking();
        $this->dogs = new Dog();
        $this->helper = new Helpers();
	}

	/** @test */
	public function getBookings() {
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['clientid'], 1);
		$this->assertEquals($results[0]['price'], 200);
		$this->assertEquals($results[0]['checkindate'], '2021-08-04 15:00:00');
		$this->assertEquals($results[0]['checkoutdate'], '2021-08-11 15:00:00');
	}

    /** @test */
    public function addBookings() {
        $bookingList = $this->booking->getBookings();
        $lastId = $bookingList[0]['id'];
        foreach($bookingList as $booking) {
            if ($booking['id'] > $lastId)
                $lastId = $booking['id'];
        }

        $this->booking->addBooking([], 100, '2021-08-04 15:00:00',  '2021-08-11 15:00:00', 1);
        $bookingList = $this->booking->getBookings();

        $this->assertIsArray($bookingList);
        $this->assertIsNotObject($bookingList);

        $this->assertEquals($bookingList[count($bookingList)-1]['id'], $lastId+1);
        $this->assertEquals($bookingList[count($bookingList)-1]['clientid'], 1);
        $this->assertEquals($bookingList[count($bookingList)-1]['price'], 100);
        $this->assertEquals($bookingList[count($bookingList)-1]['checkindate'], '2021-08-04 15:00:00');
        $this->assertEquals($bookingList[count($bookingList)-1]['checkoutdate'], '2021-08-11 15:00:00');
    }

    /** @test */
    public function addBookingsWithDiscount() {
        $bookingList = $this->booking->getBookings();
        $lastId = $bookingList[0]['id'];
        foreach($bookingList as $booking) {
            if ($booking['id'] > $lastId)
                $lastId = $booking['id'];
        }

        $clientid = 1;
        $dogList = $this->dogs->getDogs();
        $dogsData = [];
        foreach ($dogList as $dog) {
            if ($dog['clientid'] == $clientid) {
                $dogsData[] = $dog;
            }
        }

        $newBooking = $this->booking->addBooking($dogsData, 100, '2021-08-04 15:00:00',  '2021-08-11 15:00:00', $clientid);
        $this->assertEquals(90, $newBooking['price']);

        $clientid2 = 4;
        $dogList = $this->dogs->getDogs();
        $dogsData2 = [];
        foreach ($dogList as $dog) {
            if ($dog['clientid'] == $clientid2) {
                $dogsData2[] = $dog;
            }
        }

        $newBooking = $this->booking->addBooking($dogsData2, 100, '2021-08-04 15:00:00',  '2021-08-11 15:00:00', $clientid2);
        $this->assertEquals(100, $newBooking['price']);
    }
}
