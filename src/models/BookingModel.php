<?php

namespace Src\models;

use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;

	function __construct() {
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings() {
		return $this->bookingData;
	}

    public function saveBookings() {
        $helper = new Helpers();
        $helper->putJson($this->bookingData, 'bookings');
    }

    public function addBooking($dogsData, $price, $checkInDate, $checkOutDate, $client) {

        $id = $this->bookingData[0]['id'];
        foreach($this->bookingData as $booking) {
            if ($booking['id'] > $id)
                $id = $booking['id'];
        }
        $id++;

        if (count($dogsData) > 0) {
            $dogAgeSum = 0;
            foreach($dogsData as $dog) {
                $dogAgeSum+=$dog['age'];
            }

            if ($dogAgeSum/count($dogsData) < 10) {
                $price = $price * 0.9;
            }
        }

        $newBooking = [
            'id' => $id,
            'clientid' => $client,
            'price' => $price,
            'checkindate' => $checkInDate,
            'checkoutdate' => $checkOutDate,
        ];

        $this->bookingData[] = $newBooking;
        $this->saveBookings();
        return $newBooking;
    }
}
